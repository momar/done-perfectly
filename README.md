# Done Perfectly

The aim of this project is nothing less than to build a perfect todo list. You all know it as an example for a first project in a new programming language; this project takes that to completion!

- Extremely fast, with live collaboration that feels natural and doesn't break
- Layout & UX designed from the ground up to work on every device as intuitively as possible
- Good accessibility e.g. for screen readers, slow/unstable internet connections & alternative input devices
- Fully internationalized (German & English for a start) with [gettext]()-compatible translation files managed in a web-based translation tool
- Using best practices everywhere if they make sense - if I missed an important one, please open an issue
- Test-driven development with 100% code coverage with unit tests, also UI & integration tests
- Fully using [Git Flow]() with [Semantic Commits]()
- Using modern languages & frameworks like [Vue 3]() and [Go]()
- Well-documented REST API, and web sockets for live collaboration
- Progressive web app with offline change tracking
- Modern & flexible authentication using an OAuth2 provider of your choice, using [Dex]() as an example
- Packaged as an OCI container image for easy deployment with [Docker]() or [Podman]()
- Continuous integration & deployment, using [Woodpecker]()
- A public server for testing: https://done.perfectly.app

Regarding **features**, this offers the following:
- **Multiple "projects"**, each of them containing a single list
- **Sub-tasks** and generally deeply nested tasks
- **Sharing lists** by link or by email, with different permissions (read, write, manage)
- **Add details** like a description, assigned people, a deadline, tags & a status to tasks
- **Notifications** for deadlines, new tasks, etc., using the [Notifications API]()
- **Order items** using drag & drop
- **Anonymous lists** created by guest users, with one link for each access type (read, write and manage)

These features are explicitly **out of scope** for this projects:
- **A kanban board or generally other views** - the view the user sees out of the box should work for most use cases already
- **Generally more advanced project management features (like advanced time tracking)** - this should stay simple & universal, catering towards a single use-case introduces unnecessary complexity, there are better tools for that
- **Intelligent (or auto-generated) lists** - it should give the user control, and take away complexity; automatically generated lists are automatically quite complex

## Architecture
- Frontend (`ui`)
  - Vue 3
  - Naive UI
- Backend (`server`)
  - Go ≥1.18
  - Fiber
  - PostgreSQL
- Documentation (`docs`)
  - MkDocs 1
  - Material for MkDocs

## Roadmap
- [ ] Structure, with backend, frontend, database, tests, docs, container image & CI configuration
- [ ] UI Design draft
- [ ] Single list of tasks per server
- [ ] Projects with weekly budgets
- [ ] Nested tasks
- [ ] Reorderable lists
- [ ] Other Features (TODO)
- [ ] Hosted version (without account)
- [ ] Premium version (with collaboration & multi-device account, probably for something like 2,50 € per month)
